import React from "react";


const SampleFun = (props) => {
            return(
                <div>Hi {props.name} a.k.a {props.aliasName}. You have {props.unread === "0"? "no":props.unread} unread {props.unread === "1"? "message" : "messages"}.</div>
            )
        }

export default SampleFun;
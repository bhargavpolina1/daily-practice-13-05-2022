import './App.css';
import SampleFun from './components/SampleComponent';

function App() {
  return (
    <div className="App">
      <SampleFun name = "Vicky" aliasName = "Vikram" unread = "0"/>
      <SampleFun name = "Raman" aliasName = "Rama Krishna" unread = "1"/>
      <SampleFun name = "Kushi" aliasName = "Kushitha"  unread = "2"/>
    </div>
  );
}

export default App;
